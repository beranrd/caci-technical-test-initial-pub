package com.bamofah.brickstore.orderinfo;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class OrderInfoRowMapperTest {

    @Mock
    private ResultSet mockResultSet;

    private OrderInfoRowMapper orderInfoRowMapper;

    private static final Long ORDER_REF = 100L;
    private static final int ORDER_QTY = 123;

    @Before
    public void setUp() {

        initMocks(this);

        orderInfoRowMapper = new OrderInfoRowMapper();
    }

    @Test
    public void shouldCreateOrderInfoFromRowMapperUsingResultsSet() throws SQLException {

        when(mockResultSet.getObject("orderRef", Long.class)).thenReturn(ORDER_REF);
        when(mockResultSet.getObject("orderQty", Integer.class)).thenReturn(ORDER_QTY);

        OrderInfo orderInfo = orderInfoRowMapper.mapRow(mockResultSet, 0);

        assertNotNull("order info should have been created", orderInfo);
        assertEquals("Order Ref mapped correctly", ORDER_REF, orderInfo.getOrderRef());
        assertEquals("Order quantity mapped correctly", ORDER_QTY, orderInfo.getOrderQty());
    }

}