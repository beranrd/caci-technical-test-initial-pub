package com.bamofah.brickstore.orderinfo;

import com.bamofah.brickstore.common.exception.BadRequestException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

import static org.mockito.MockitoAnnotations.initMocks;

public class OrderInfoControllerTest {

    @InjectMocks
    private OrderInfoController orderInfoController;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestWhenInvalidOrderref() {

        orderInfoController.getOrderInfo(null);
    }

}