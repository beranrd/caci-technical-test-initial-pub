package com.bamofah.brickstore.createorder;

import com.bamofah.brickstore.common.exception.BadRequestException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.MockitoAnnotations.initMocks;

public class CreateOrderControllerTest {

    @InjectMocks
    private CreateOrderController instance;

    @Mock
    private CreateOrderService mockCreateOrderService;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
    }


    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestIfNoOrZeroOrderQtyIsProvidedWhenCreatingOrder() {

        instance.createOrder(0);
    }
}