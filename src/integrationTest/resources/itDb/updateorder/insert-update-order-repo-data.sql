INSERT INTO bricks (id, order_created, order_quantity)
VALUES
    (200, now(), 15),
    (300, date_add(now(), INTERVAL 5 DAY), 1);