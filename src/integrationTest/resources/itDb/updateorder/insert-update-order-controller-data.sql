INSERT INTO bricks (id, order_created, order_quantity)
VALUES
    (200, date_add(now(), INTERVAL 5 DAY), 15),
    (300, date_add(now(), INTERVAL 5 MONTH ), 1);

INSERT INTO bricks (id, order_created, order_quantity, is_dispatched)
VALUES
    (210, now(), 5, 'Y');