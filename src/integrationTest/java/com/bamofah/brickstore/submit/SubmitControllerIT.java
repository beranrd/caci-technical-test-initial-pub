package com.bamofah.brickstore.submit;

import com.bamofah.brickstore.IntegrationTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.jdbc.Sql;

import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql("/itDb/submit/insert-submit-controller-data.sql")
public class SubmitControllerIT extends IntegrationTest {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final String CHECK_ORDER_STATUS = "select " +
        "CASE " +
        "   WHEN br.is_dispatched = 'Y' THEN 'true' " +
        "   ELSE 'false' " +
        "END " +
        "from bricks br where br.id = :orderRef;";


    @Test
    public void shouldSetOrderToDispatchStatus() throws Exception {

        mockMvc.perform(put("/orders/1"))
            .andDo(print())
            .andExpect(status().isNoContent());

        Map<String, Long> param = Collections.singletonMap("orderRef", 1L);
        boolean isDispatched = namedParameterJdbcTemplate.queryForObject(CHECK_ORDER_STATUS, param, Boolean.class);

        assertTrue("order should be in dispatched status", isDispatched);
    }

    @Test
    public void shouldReturnBadRequestWhenOrderRefDosNotExistDuringSubmission() throws Exception {

        mockMvc.perform(put("/orders/50"))
            .andDo(print())
            .andExpect(status().isNotFound());
    }
}