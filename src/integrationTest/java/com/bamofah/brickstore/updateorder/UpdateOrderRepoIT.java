package com.bamofah.brickstore.updateorder;

import com.bamofah.brickstore.IntegrationTest;
import com.bamofah.brickstore.common.exception.ConflictException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.jdbc.Sql;

import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Sql("/itDb/updateorder/insert-update-order-repo-data.sql")
public class UpdateOrderRepoIT extends IntegrationTest {

    @Autowired
    private UpdateOrderRepo updateOrderRepo;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    private static final Long VALID_ORDER = 200L;
    private static final Long INVALID_ORDER = 1L;


    @Test(expected = ConflictException.class)
    public void shouldReturnNotFoundExceptionWhenNoOrdersFoundDaringUpdate() {

        updateOrderRepo.updateOrder(INVALID_ORDER, 16);

    }

    @Test
    public void shouldSuccessfullyUpdateOrder() {

        int updateQty = 60;
        String checkUpdateQuery = "select br.order_quantity from bricks br where br.id = :orderRef";
        Map<String, Long> param = Collections.singletonMap("orderRef", VALID_ORDER);
        updateOrderRepo.updateOrder(VALID_ORDER, updateQty);

        int actualQty = namedParameterJdbcTemplate.queryForObject(checkUpdateQuery, param, Integer.class);

        assertEquals("order quantity should have been updated", updateQty, actualQty);
    }


}