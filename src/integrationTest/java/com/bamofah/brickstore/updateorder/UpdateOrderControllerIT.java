package com.bamofah.brickstore.updateorder;

import com.bamofah.brickstore.IntegrationTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql("/itDb/updateorder/insert-update-order-controller-data.sql")
public class UpdateOrderControllerIT extends IntegrationTest {

    private static final Long ORDER_REF = 300L;
    @Autowired
    private ObjectMapper objectMapper;
    private String json;

    @Test
    public void shouldReturnBadRequestWhenNoOrderFoundDuringUpdate() throws Exception {

        json = generateReqJson(600L, 1);
        mockMvc.perform(put("/orders").content(json).contentType(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isBadRequest());

    }

    private String generateReqJson(Long orderRef, int orderQty) throws JsonProcessingException {

        UpdateOrderBody orderBody = new UpdateOrderBody();
        orderBody.setOrderRef(orderRef);
        orderBody.setOrderQty(orderQty);

        return objectMapper.writeValueAsString(orderBody);
    }

    @Test
    public void shouldUpdateOrderQuantityAndReturnOrderRef() throws Exception {

        json = generateReqJson(ORDER_REF, 30);
        mockMvc.perform(put("/orders").content(json).contentType(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(jsonPath("$.orderRef").value(ORDER_REF))
            .andExpect(status().isNoContent());
    }

    @Test
    public void shouldReturnBadRequestWhenUpdatingAlreadyDispatchedOrder() throws Exception {

        json = generateReqJson(210L, 9);
        mockMvc.perform(put("/orders").content(json).contentType(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isBadRequest());
    }
}