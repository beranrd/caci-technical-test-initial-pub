package com.bamofah.brickstore.createorder;

import com.bamofah.brickstore.IntegrationTest;
import org.junit.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CreateOrderControllerIT extends IntegrationTest {

    @Test
    public void shouldHaveCreatedOrderAndReturnUniqueRef() throws Exception {

        int orderQty = 10;
        mockMvc.perform(post("/orders/" + orderQty))
               .andDo(print())
               .andExpect(status().isCreated())
               .andExpect(jsonPath("orderRef").isNumber())
        ;
    }


}