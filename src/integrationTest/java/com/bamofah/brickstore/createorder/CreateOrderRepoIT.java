package com.bamofah.brickstore.createorder;

import com.bamofah.brickstore.IntegrationTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CreateOrderRepoIT extends IntegrationTest {

    @Autowired
    private CreateOrderRepo createOrderRepo;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private static final String GET_ORDERED_QUANTITY = "SELECT br.order_quantity " +
        "FROM bricks br " +
        "WHERE br.id = :orderRef;";


        @Test
    public void shouldCreateNewOrder(){

        Long newOrderId = createOrderRepo.createOrder(16);
        assertNotNull("a new order should have been created", newOrderId);
    }

    @Test
    public void shouldHaveCorrectOrderQuantityForCreatedOrder(){

        int orderedQty = 20;

        Long newOrderId = createOrderRepo.createOrder(orderedQty);

        int storedQty = getOrderedQty(newOrderId);

        assertEquals("order quantity added correctly", orderedQty, storedQty);

    }


    private int getOrderedQty(Long orderRef) {

        Map<String, Long> param = Collections.singletonMap("orderRef", orderRef);
        return namedParameterJdbcTemplate.queryForObject(GET_ORDERED_QUANTITY, param, Integer.class);
    }

}