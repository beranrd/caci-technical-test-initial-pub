package com.bamofah.brickstore.common;

import com.bamofah.brickstore.IntegrationTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Sql("/itDb/common/insert-checkorderrepo-data.sql")
public class OrderCheckRepoIT extends IntegrationTest {

    @Autowired
    private OrderCheckRepo checkRepo;

    private static final Long VALID_ORDER = 200L;
    private static final Long INVALID_ORDER = 1L;

    @Test
    public void shouldReturnFalseWhenNoOrderFound() {

        boolean orderExist = checkRepo.orderExist(INVALID_ORDER);
        assertFalse("order doesn't exist", orderExist);
    }

    @Test
    public void shouldReturnTrueWhenOrderIsFound() {

        boolean orderExist = checkRepo.orderExist(VALID_ORDER);
        assertTrue("order exists", orderExist);
    }

    @Test
    public void shouldReturnFalseWhenIsNotDispatched() {

        boolean orderExist = checkRepo.alreadyDispatched(VALID_ORDER);
        assertFalse("order not dispatched", orderExist);
    }

    @Test
    public void shouldReturnTrueWhenOrderIsDispatched() {

        boolean orderExist = checkRepo.alreadyDispatched(210L);
        assertTrue("order dispatched", orderExist);
    }

}