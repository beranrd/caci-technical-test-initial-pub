package com.bamofah.brickstore.updateorder;

class UpdateOrderBody {

    private Long orderRef;
    private int orderQty;

    public void setOrderRef(Long orderRef) {
        this.orderRef = orderRef;
    }

    public Long getOrderRef() {
        return orderRef;
    }

    public void setOrderQty(int orderQty) {
        this.orderQty = orderQty;
    }

    public int getOrderQty() {
        return orderQty;
    }
}
