package com.bamofah.brickstore.updateorder;

import com.bamofah.brickstore.common.OrderCheckRepo;
import com.bamofah.brickstore.common.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
class UpdateOrderService {

    @Autowired
    private UpdateOrderRepo updateOrderRepo;

    @Autowired
    private OrderCheckRepo checkRepo;

    public void updateOrder(Long orderRef, int orderQty) {

        if(!checkRepo.orderExist(orderRef)){
            throw new BadRequestException("Update failed - No order with ref " + orderRef);
        } else if (checkRepo.alreadyDispatched(orderRef)) {
            throw new BadRequestException("Update failed - Order already dispatched");
        }

        updateOrderRepo.updateOrder(orderRef,orderQty);
    }
}
