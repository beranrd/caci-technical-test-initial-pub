package com.bamofah.brickstore.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;


public class MvcWebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {


    /**
     * This specifies the configuration class forthe root
     * application context
     *
     * @return array of  root application configs
     */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{ApplicationConfig.class};
    }


    /**
     * This specifies the configuration classes for the dispatcher
     * servlet application context
     *
     * @return
     */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return null;
    }

    /**
     * This specifies the servlet mappings for DispatcherServlet
     *
     * @return
     */
    @Override
    public String[] getServletMappings() {

        return new String[]{"/"};
    }
}
