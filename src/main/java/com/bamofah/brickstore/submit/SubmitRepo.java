package com.bamofah.brickstore.submit;

import com.bamofah.brickstore.common.exception.ConflictException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.Map;

@Repository
class SubmitRepo {

    private static final Logger LOG = LoggerFactory.getLogger(SubmitRepo.class);

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final String DISPATCH_ORDER_QUERY = "update bricks set is_dispatched = 'Y' where id = :orderRef;";

    public void submitOrder(Long orderRef) {

        LOG.debug("Submitting order {}", orderRef);
        Map<String, Long> param = Collections.singletonMap("orderRef", orderRef);

        int rowsAffected = namedParameterJdbcTemplate.update(DISPATCH_ORDER_QUERY, param);

        if (rowsAffected <= 0) {
            LOG.warn("No order submitted for order {}", orderRef);
            throw new ConflictException("Failed to submit order " + orderRef);
        }
    }
}
