package com.bamofah.brickstore.submit;

import com.bamofah.brickstore.common.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SubmitController {

    @Autowired
    private SubmitService submitService;

    @PutMapping("/orders/{orderRef}")
    public ResponseEntity submitOrder(@PathVariable Long orderRef){

        validateOrderRef(orderRef);
        submitService.submitOrder(orderRef);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    private void validateOrderRef(Long orderRef){

        if(orderRef == null) {
            throw new BadRequestException("Unable to submit order - Invalid order ref");
        }
    }
}
