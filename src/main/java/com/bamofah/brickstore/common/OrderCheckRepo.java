package com.bamofah.brickstore.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.Map;

@Repository
public class OrderCheckRepo {

    private static final Logger LOG = LoggerFactory.getLogger(OrderCheckRepo.class);

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final String CHECK_ORDER_STRING = "select count(*) from bricks where id = :orderRef; ";

    private final String CHECK_ORDER_STATUS = "select " +
        "CASE " +
        "   WHEN br.is_dispatched = 'Y' THEN 'true' " +
        "   ELSE 'false' " +
        "END " +
        "from bricks br where br.id = :orderRef;";

    public boolean orderExist(Long orderRef) {

        LOG.debug("checking if order exists");
        Map<String, Long> param = Collections.singletonMap("orderRef", orderRef);
        return namedParameterJdbcTemplate.queryForObject(CHECK_ORDER_STRING,param,Integer.class) == 1;
    }

    public boolean alreadyDispatched(Long orderRef) {

        LOG.debug("Checking dispatch status for order {}", orderRef);
        Map<String, Long> param = Collections.singletonMap("orderRef", orderRef);
        return namedParameterJdbcTemplate.queryForObject(CHECK_ORDER_STATUS, param, Boolean.class);
    }
}
