package com.bamofah.brickstore.common;


import org.springframework.jdbc.core.RowMapper;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public abstract class MyBaseRowMapper<T> implements RowMapper<T> {

    protected Boolean getYNBoolean(ResultSet rs, String column) throws SQLException {
        String result = rs.getString(column);
        if ("Y".equalsIgnoreCase(result)) {
            return true;
        }
        if ("N".equalsIgnoreCase(result)) {
            return false;
        }
        return null;
    }

    protected LocalDate getLocalDate(ResultSet rs, String field) throws SQLException {

        Date date = rs.getDate(field);

        if (date == null) {
            return null;
        }

        return date.toLocalDate();
    }

    protected Integer getInteger(ResultSet rs, String column) throws SQLException {
        Integer result = rs.getObject(column, Integer.class);
        return rs.wasNull() ? null : result;
    }

    protected Long getLong(ResultSet rs, String column) throws SQLException {
        Long result = rs.getObject(column, Long.class);
        return rs.wasNull() ? null : result;
    }
}
