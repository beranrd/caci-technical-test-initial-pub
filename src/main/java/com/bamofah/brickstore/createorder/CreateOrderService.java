package com.bamofah.brickstore.createorder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
class CreateOrderService {

    @Autowired
    private CreateOrderRepo createOrderRepo;

    public Long createOrder(int orderQty) {

        return createOrderRepo.createOrder(orderQty);
    }
}
