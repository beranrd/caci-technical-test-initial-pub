package com.bamofah.brickstore.createorder;

import com.bamofah.brickstore.common.exception.ConflictException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Repository
class CreateOrderRepo {


    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private static final Logger LOG = LoggerFactory.getLogger(CreateOrderRepo.class);

    private static final String CREATE_ORDER_QUERY = "INSERT INTO bricks (order_created, order_quantity) " +
        "VALUES (:dateCreated, :orderQty); ";

    public Long createOrder(int orderedQty) {


        LOG.debug("adding new bricks order with a qty of {}", orderedQty);

        MapSqlParameterSource params = new MapSqlParameterSource();
        KeyHolder keyHolder = new GeneratedKeyHolder();
        LocalDateTime now = LocalDateTime.now();

        params.addValue("dateCreated", Timestamp.valueOf(now));
        params.addValue("orderQty", orderedQty);

        namedParameterJdbcTemplate.update(CREATE_ORDER_QUERY, params, keyHolder, new String[]{"id"});

        Long orderRef = keyHolder.getKey().longValue();

        if (orderRef == null) {
            LOG.warn("Failed to create the order");
            throw new ConflictException("Failed to create new brick order");
        } else {
            return orderRef;
        }

    }
}
